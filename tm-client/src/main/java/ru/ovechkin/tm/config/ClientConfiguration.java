package ru.ovechkin.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ovechkin.tm.endpoint.*;

@Configuration
@ComponentScan("ru.ovechkin.tm")
public class ClientConfiguration {

    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    public UserEndpoint userEndpoint() {
        return userEndpointService().getUserEndpointPort();
    }


    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        return sessionEndpointService().getSessionEndpointPort();
    }


    @Bean
    public StorageEndpointService storageEndpointService() {
        return new StorageEndpointService();
    }

    @Bean
    public StorageEndpoint storageEndpoint() {
        return storageEndpointService().getStorageEndpointPort();
    }


    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint() {
        return projectEndpointService().getProjectEndpointPort();
    }


    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean TaskEndpoint taskEndpoint() {
        return taskEndpointService().getTaskEndpointPort();
    }

}