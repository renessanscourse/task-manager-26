package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class ServicePortCommand extends AbstractCommand {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SERVER_PORT;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server port";
    }

    @Override
    public void execute() {
        System.out.println("[PORT]");
        System.out.println(storageEndpoint.getServerPortInfo());
        System.out.println("[OK]");
    }

}
