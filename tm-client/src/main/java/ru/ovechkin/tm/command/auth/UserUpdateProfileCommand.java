package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.SessionEndpoint;
import ru.ovechkin.tm.endpoint.UserDTO;
import ru.ovechkin.tm.endpoint.UserEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.UPDATE_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Update you profile";
    }

    @Override
    public void execute() {
        @Nullable final UserDTO userDTO = sessionEndpoint.getUser(sessionDTO);
        System.out.println("[UPDATE PROFILE INFO]");
        System.out.println("YOUR CURRENT LOGIN IS: " + "[" + userDTO.getLogin() + "]");
        System.out.print("ENTER NEW LOGIN: ");
        final String newLogin = TerminalUtil.nextLine();
        userEndpoint.updateProfileLogin(sessionDTO, newLogin);
        System.out.println("[COMPLETE]");
    }

}