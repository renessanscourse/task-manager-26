package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.endpoint.UserEndpoint;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class UserRemoveCommand extends AbstractCommand {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user's account by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.removeByLogin(sessionDTO, login);
        System.out.println("[OK]");
    }

}