package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_ABOUT;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_ABOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

}