package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_CREATE;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.print("ENTER THE PROJECT ID TO WHICH YOU WANT TO ADD THE TASK: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        taskEndpoint.createTaskWithNameAndDescription(sessionDTO, name, description, projectId);
        System.out.println("[OK]");
    }

}