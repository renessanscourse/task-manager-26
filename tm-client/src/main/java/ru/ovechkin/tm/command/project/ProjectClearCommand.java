package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectEndpoint;

@Component
public final class ProjectClearCommand extends AbstractCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.removeAllProjects(sessionDTO);
        System.out.println("[OK]");
    }

}