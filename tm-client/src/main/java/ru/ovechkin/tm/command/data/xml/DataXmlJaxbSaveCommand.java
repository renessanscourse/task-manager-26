package ru.ovechkin.tm.command.data.xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_XML_JAXB_SAVE;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");
        storageEndpoint.dataXmlJaxbSave(sessionDTO);
        System.out.println("[OK]");
    }

}