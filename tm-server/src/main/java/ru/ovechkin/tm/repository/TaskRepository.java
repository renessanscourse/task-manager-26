package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TaskRepository implements ITaskRepository {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public final void add(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND id = :id", Task.class)
                .setMaxResults(1);
        query.setParameter("userId", userId);
        query.setParameter("id", id);
        @NotNull final Task task = query.getSingleResult();
        return task;
    }

    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND name = :name", Task.class)
                .setMaxResults(1);
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        @NotNull final Task task = query.getSingleResult();
        return task;
    }

    @Nullable
    @Override
    public final List<Task> findUserTasks(@NotNull final String userId) {
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId", Task.class);
        query.setParameter("userId", userId);
        if (query.getResultList() == null || query.getResultList().isEmpty()) return null;
        @Nullable final List<Task> tasks = query.getResultList();
        return tasks;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final Query query = entityManager.createQuery(
                "DELETE Task WHERE user_id = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        entityManager.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        entityManager.remove(task);
        return task;
    }


    @Nullable
    @Override
    public List<Task> getAllTasks() {
        @Nullable final TypedQuery<Task> query = entityManager.createQuery("FROM Task", Task.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> mergeCollection(@NotNull final List<Task> taskList) {
        for (@NotNull final Task task : taskList) {
            entityManager.persist(task);
        }
        return taskList;
    }

    @Override
    public void removeAllTasks() {
        final Query query = entityManager.createQuery("DELETE FROM Task", Task.class);
        query.executeUpdate();
    }

}