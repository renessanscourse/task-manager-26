package ru.ovechkin.tm.exeption.empty;

public class NameEmptyException extends RuntimeException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}