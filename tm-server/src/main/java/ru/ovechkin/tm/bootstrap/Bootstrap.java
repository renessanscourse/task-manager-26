package ru.ovechkin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.endpoint.*;

import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {

    public Bootstrap() {
    }

    @Autowired
    private AnnotationConfigApplicationContext context;

    @Autowired
    private IPropertyService propertyService;

    public void init() throws Exception {
        initProperty();
        initEndpoint();
    }

    private void initProperty() throws Exception {
        propertyService.init();
    }

    private void initEndpoint() {
        registry(context.getBean(UserEndpoint.class));
        registry(context.getBean(SessionEndpoint.class));
        registry(context.getBean(StorageEndpoint.class));
        registry(context.getBean(TaskEndpoint.class));
        registry(context.getBean(ProjectEndpoint.class));
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServiceHost();
        @NotNull final Integer port = propertyService.getServicePort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}