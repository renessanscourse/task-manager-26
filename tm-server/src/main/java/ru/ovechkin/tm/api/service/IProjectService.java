package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void add(@Nullable SessionDTO sessionDTO, @Nullable ProjectDTO project);

    void create(@Nullable SessionDTO sessionDTO, @Nullable String name);

    void create(@Nullable SessionDTO sessionDTO, @Nullable  String name, @Nullable String description);

    @NotNull
    List<ProjectDTO> findUserProjects(@Nullable String userId);

    void removeAllUserProjects(@Nullable String userId);

    @NotNull
    ProjectDTO findProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    ProjectDTO removeProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO removeProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull List<ProjectDTO> getAllProjectsDTO();

    @NotNull List<ProjectDTO> loadProjects(@Nullable List<ProjectDTO> projectsDTO);

    void removeAllProjects();
}