package ru.ovechkin.tm.api.service;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IStorageService {

    void dataBase64Save() throws Exception;

    void dataBase64Load() throws Exception;

    void dataBinarySave() throws IOException;

    void dataBinaryLoad() throws IOException, ClassNotFoundException;

    void dataJsonJaxbSave() throws IOException, JAXBException;

    void dataJsonJaxbLoad() throws JAXBException;

    void dataJsonMapperLoad() throws IOException;

    void dataJsonMapperSave() throws IOException;

    void dataXmlJaxbSave() throws IOException, JAXBException;

    void dataXmlJaxbLoad() throws IOException, JAXBException;

    void dataXmlMapperSave() throws IOException;

    void dataXmlMapperLoad() throws IOException;

}